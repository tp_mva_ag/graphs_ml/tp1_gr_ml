from skimage import io
import time
from spectral_clustering.spctr_clstr_func import *


def image_segmentation(input_img):
    #      a skeleton function to perform image segmentation, needs to be completed
    #  Input
    #  input_img:
    #      (string) name of the image file, without extension (e.g. 'four_elements.bmp')
    print('start of image segmentation : duration on an Intel i5 is about 2mn30''')

    filename = input_img
    x = []

    try:
        x = io.imread(filename)
    except FileNotFoundError:
        try:
            filename = '../' + input_img
            x = io.imread(filename)
        except FileNotFoundError:
            print('File ' + input_img + ' not found')

    # On ramène les valeurs de x entre 0 et 1
    x = (x - np.min(x)) / (np.max(x) - np.min(x))

    im_side = np.size(x, 1)
    xr = x.reshape(im_side ** 2, 3)

    #################################################################
    # y_rec should contain an index from 1 to c where c is the      #
    # number of segments you want to split the image into           #
    #################################################################

    print('Beginning of computation')
    start_time = time.time()

    var = 0.03

    w = build_w(xr, var)
    laplacian = build_laplacian(w, 'rw')
    y_rec = spectral_clustering_adaptive(laplacian)

    print('end of computation')
    print('Duration : %s seconds' % (time.time() - start_time))

    #################################################################
    #################################################################

    plt.figure()

    plt.subplot(1, 2, 1)
    plt.imshow(x)

    plt.subplot(1, 2, 2)
    y_rec = y_rec.reshape(im_side, im_side)
    plt.imshow(y_rec)

    plt.show()


image_segmentation('./data/fruit_salad.bmp')
image_segmentation('./data/four_elements.bmp')
# 266s with np.linalg.eig
