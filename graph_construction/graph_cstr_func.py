import scipy.spatial.distance as sd
import sys
import os
from helper import *
from graph_construction.generate_data import *

sys.path.append(os.path.dirname(os.getcwd()))


def build_similarity_graph(x, var=0.2, eps=0., k=0):
    """Computes the similarity matrix for a given dataset of samples.

     Input
     X -- (n x m) matrix of m-dimensional samples
     k and eps -- controls the main parameter of the graph, the number
         of neighbours k for k-nn, and the threshold eps for epsilon graphs
         If k=eps=0, the returned graph is fully-connected
     var -- the sigma value for the exponential function, already squared

     Output
     W -- (n x n) dimensional matrix representing the adjacency matrix of the graph
         similarities: (n x n) dimensional matrix containing
     similarities -- all the similarities between all points (optional output)
    """

    #  The similarity function is d(x,y)=exp(-||x-y||^2/var)
    similarities = sd.cdist(x, x,
                            lambda u, y: np.exp(-1 * np.sqrt(((u - y) ** 2).sum()) / var))

    # Then we compute the weight matrix with epsilon or k-nn

    if eps:
        #################################################################
        #  compute an epsilon graph from the similarities               #
        #  for each node x_i, an epsilon graph has weights              #
        #  w_ij = d(x_i,x_j) when w_ij > eps, and 0 otherwise           #
        #################################################################
        w = (similarities >= eps) * similarities

        return w, similarities

    elif k:
        #################################################################
        #  compute a k-nn graph from the similarities                   #
        #  for each node x_i, a k-nn graph has weights                  #
        #  w_ij = d(x_i,x_j) for the k closest nodes to x_i, and 0      #
        #  for all the k-n remaining nodes                              #
        #  Remember to remove self similarity and                       #
        #  make the graph undirected                                    #
        #################################################################

        """For each line, we sort the weights by descending order, then we get the
        weight of the k-th node. We go back to the original matrix and put to 0
        all the nodes which weight is inferior.
        """

        w = np.copy(similarities)   # So w is the same size than similarities
        for i in range(similarities[0].size):
            line = np.array(similarities[i])
            line[::-1].sort()  # Sorting i descending order
            threshold = line[k]

            w[i] = (similarities[i] >= threshold) * similarities[i]

        return w, similarities

    # If we choose neither eps nor k, w = similarities
    else:
        return similarities, similarities


def plot_similarity_graph(x, y, eps=0., k=0, var = 0.2):
    """a skeleton function to analyze the construction of the graph similarity matrix"""

    w, similarity = build_similarity_graph(x, var, eps, k)  # We build the weight and similarities matrices

    plot_graph_matrix(x, y, w)  # We plot the ground truth and the computed similarities


def how_to_choose_epsilon(gen_pam=1., var=0.2):
    """a skeleton function to analyze the influence of the graph structure on the epsilon graph matrix"""

    num_samples = 100

    [x, y] = worst_case_blob(num_samples, gen_pam)  # We create the dataset with the worst_case_blob fucntion, which is
    # a blob with an outer which is at a distance gen_pam from the main blob (and which risk to be unconnected.

    #################################################################
    # use the similarity function and the max_span_tree function    #
    # to build the maximum spanning tree max_tree                   #
    # sigma2: the exponential_euclidean's sigma2 parameter          #
    # similarities: (n x n) matrix with similarities between        #
    #              all possible couples of points                   #
    # max_tree: (n x n) indicator matrix for the edges in           #
    #           the maximum spanning tree                           #
    #################################################################

    w, similarities = build_similarity_graph(x, var)
    max_tree = max_span_tree(similarities)

    #################################################################
    # set graph_thresh to the minimum weight in max_tree            #
    #################################################################

    eps = np.min((max_tree * similarities)[np.nonzero(max_tree)])   # eps wil be the minimum distance to make all nodes
    # connected

    plot_similarity_graph(x, y, eps)
    print('Epsilon optimal = %s' % eps)
    return eps
