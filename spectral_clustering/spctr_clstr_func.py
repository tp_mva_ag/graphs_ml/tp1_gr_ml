import sklearn.cluster as skc
import sklearn.metrics as skm
import scipy as scp
import scipy.sparse.linalg
import scipy.io as sio
from graph_construction.graph_cstr_func import *


def build_w(x, var, epsilon=0., k=0):
    """Returns the weight matrix computed with epsilon"""

    w, similarity = build_similarity_graph(x, var, epsilon, k)
    return w


def build_laplacian(w, laplacian_normalization="unn"):
    """Compute the Laplacian from a weight matrix

    laplacian_normalization -- string selecting which version of the laplacian matrix to construct either
        'unn'normalized,
        'sym'metric normalization or
        'rw' random-walk normalization

    Outputs
    laplacian -- (n x n) dimensional matrix representing the Laplacian of the graph
    """

    degree_mat = w.dot(np.ones(w.shape)) * np.eye(w.shape[0])
    laplacian = degree_mat - w

    if laplacian_normalization == "sym":
        d_sqrt = scp.linalg.sqrtm(degree_mat)
        d_sqrt_inv = np.linalg.inv(d_sqrt)
        laplacian = (d_sqrt_inv.dot(laplacian)).dot(d_sqrt_inv)

    elif laplacian_normalization == "rw":
        d_inv = np.linalg.inv(degree_mat)
        laplacian = d_inv.dot(laplacian)

    return laplacian


def spectral_clustering(laplacian, chosen_eig_indices, num_classes=2):
    """ Create a spectral clustering from the given laplacian

    Input
    laplacian -- Graph Laplacian (standard or normalized)
    chosen_eig_indices -- indices of eigenvectors to use for clustering
    num_classes -- number of clusters to compute (defaults to 2)

    Output
    y -- Cluster assignments
    """

    #################################################################
    # compute eigenvectors                                          #
    # eigen_values = (1 x n) eigenvalue vector (sorted)             #
    # v = (n x n) eigenvector matrix                                #
    #################################################################

    # We get the eigen values and vectors, not sorted
    eigen_values, v = np.linalg.eig(laplacian)

    # We sort the eigenvalues and vectors
    idx = eigen_values.argsort()
    # eigen_values = eigen_values[idx]
    v = v[:, idx]

    #################################################################
    # compute the clustering assignment from the eigenvector        #
    # y = (n x 1) cluster assignments [1,2,...,c]                   #
    #################################################################

    choosen_eig_vectors = np.real(v[:, chosen_eig_indices])
    # We use Kmeans to clusters our choosen eigenvectors
    kmeans = skc.KMeans(num_classes).fit(choosen_eig_vectors)

    # We get the labels of clusters created by kmeans
    y = kmeans.labels_

    return y


def two_blobs_clustering(k=0):
    """A skeleton function for questions 2.1,2.2"""

    in_data = []    # load the data
    try:
        in_data = sio.loadmat('data/data_2blobs.mat')
    except FileNotFoundError:
        try:
            in_data = sio.loadmat('../data/data_2blobs.mat')
        except FileNotFoundError:
            print('Impossible to find data_2blobs')

    x = in_data['X']
    y = in_data['Y']

    # automatically infer number of labels from samples
    num_classes = len(np.unique(y))

    #################################################################
    # choose the experiment parameter                               #
    #################################################################

    var = 1  # exponential_euclidean's sigma^2

    # either 'unn'normalized, 'sym'metric normalization or 'rw' random-walk normalization
    laplacian_normalization = 'unn'
    chosen_eig_indices = np.array(range(0, num_classes))  # indices of the ordered eigenvalues to pick

    #################################################################
    #################################################################

    # build laplacian
    w = build_w(x, var, 0., k)
    laplacian = build_laplacian(w, laplacian_normalization)

    y_rec = spectral_clustering(laplacian, chosen_eig_indices, num_classes)

    plot_clustering_result(x, y, laplacian, y_rec, skc.KMeans(num_classes).fit_predict(x))


def choose_eig_function(eigenvalues):
    #  [eig_ind] = choose_eig_function(eigenvalues)
    #     chooses indices of eigenvalues to use in clustering
    #
    # Input
    # eigenvalues:
    #     eigenvalues sorted in ascending order
    #
    # Output
    # eig_ind:
    #     the indices of the eigenvectors chosen for the clustering
    #     e.g. [1,2,3,5] selects 1st, 2nd, 3rd, and 5th smallest eigenvalues

    # Calcule de la 'dérivée' des valeurs propres
    eig_diff = eigenvalues[1:len(eigenvalues)] - eigenvalues[0:len(eigenvalues) - 1]

    # On repère une montée soudaine des valeurs propres
    eig_ind = np.array([0])
    for i in range(0, len(eig_diff)):
        eig_ind = np.append(eig_ind, i + 1)
        if eig_diff[i + 1] > 2 * eig_diff[i + 2] and eig_diff[i + 1] > eig_diff[i]:
            break

    print('Chosen eigenvectors indices : ')
    print(eig_ind.astype(int))
    return eig_ind.astype(int)


def spectral_clustering_adaptive(laplacian):
    #      a skeleton function to perform spectral clustering, needs to be completed
    #
    #  Input
    #  L:
    #      Graph Laplacian (standard or normalized)
    #  num_classes:
    #      number of clusters to compute (defaults to 2)
    #
    #  Output
    #  Y:
    #      Cluster assignments

    #################################################################
    # compute eigenvectors                                      #####
    # U = (n x n) eigenvector matrix                            #####
    # E = (n x n) eigenvalue diagonal matrix (sorted)           #####
    #################################################################

    # We get the eigen values and vectors, not sorted
    eigen_values, v = np.linalg.eig(laplacian)
    # eigen_values, v = scp.sparse.linalg.eigs(laplacian)

    # We sort the eigenvalues and vectors
    idx = eigen_values.argsort()
    eigen_values = eigen_values[idx]
    # v = v[:, idx]

    #################################################################
    # compute the clustering assignment from the eigenvector    #####
    # Y = (n x 1) cluster assignments [1,2,...,c]               #####
    #################################################################

    choose_eig_vectors = choose_eig_function(eigen_values)  # This time, eigen vectors are automatically choosen

    y = spectral_clustering(laplacian, choose_eig_vectors, len(choose_eig_vectors))
    return y


def find_the_bend(var=0.03):
    #      a skeleton function for question 2.3 and following, needs to be completed
    #

    # the number of samples to generate
    num_samples = 600

    [x, y] = blobs(num_samples, 4, 0.2)

    # automatically infer number of clusters from samples
    # num_classes = len(np.unique(y))

    #################################################################
    # choose the experiment parameter                               #
    #################################################################

    # either 'unn'normalized, 'sym'metric normalization or 'rw' random-walk normalization
    laplacian_normalization = 'rw'

    #################################################################
    #################################################################

    # build the laplacian
    w = build_w(x, var)
    laplacian = build_laplacian(w, laplacian_normalization)

    #################################################################
    # compute first 15 eigenvalues and apply                        #
    # eigenvalues: (n x 1) vector storing the first 15 eigenvalues  #
    #               of laplacian, sorted from smallest to largest           #
    #################################################################

    # We get the eigen values and vectors, not sorted
    eigen_values, v = np.linalg.eig(laplacian)

    # We sort the eigenvalues and vectors
    idx = eigen_values.argsort()
    eigen_values = eigen_values[idx]
    # v = v[:, idx]

    eigenvalues = eigen_values[:15]

    #################################################################
    #################################################################

    #################################################################
    # compute spectral clustering solution using a non-adaptive     #
    # method first, and an adaptive one after (see handout)         #
    # y_rec = (n x 1) cluster assignments [1,2,...,c]               #
    #################################################################

    # y_rec = spectral_clustering(laplacian, np.array(range(0, 4)), num_classes)
    y_rec = spectral_clustering_adaptive(laplacian)

    #################################################################
    #################################################################

    plot_the_bend(x, y, laplacian, y_rec, eigenvalues)


def two_moons_clustering():
    #       a skeleton function for questions 2.7

    # load the data

    in_data = []
    try:
        in_data = sio.loadmat('data/data_2moons.mat')
    except FileNotFoundError:
        try:
            in_data = sio.loadmat('../data/data_2moons.mat')
        except FileNotFoundError:
            print('Impossible to find data_2moons')

    x = in_data['X']
    y = in_data['Y']

    # automatically infer number of labels from samples
    num_classes = len(np.unique(y))

    #################################################################
    # choose the experiment parameter                               #
    #################################################################

    # k = 2
    var = 1  # exponential_euclidean's sigma^2

    # either 'unn'normalized, 'sym'metric normalization or 'rw' random-walk normalization
    laplacian_normalization = 'unn'
    chosen_eig_indices = [0, 1]  # indices of the ordered eigenvalues to pick

    #################################################################
    #################################################################

    # build laplacian
    w = build_w(x, var, 0.5)
    laplacian = build_laplacian(w, laplacian_normalization)

    y_rec = spectral_clustering(laplacian, chosen_eig_indices)

    plot_clustering_result(x, y, laplacian, y_rec, skc.KMeans(num_classes).fit_predict(x))


def point_and_circle_clustering(laplacian_normalization='rw'):
    #  [] = point_and_circle_clustering()
    #       a skeleton function for questions 2.8

    # load the data

    in_data = []
    try:
        in_data = sio.loadmat('data/data_pointandcircle.mat')
    except FileNotFoundError:
        try:
            in_data = sio.loadmat('../data/data_pointandcircle.mat')
        except FileNotFoundError:
            print('Impossible to find data_2moons')

    x = in_data['X']
    y = in_data['Y']

    # automatically infer number of labels from samples
    num_classes = len(np.unique(y))

    #################################################################
    # choose the experiment parameter                               #
    #################################################################

    # k = 2
    var = 1  # exponential_euclidean's sigma^2

    # either 'unn'normalized, 'sym'metric normalization or 'rw' random-walk normalization
    # laplacian_normalization = 'rw'
    chosen_eig_indices = np.array(range(num_classes))  # indices of the ordered eigenvalues to pick

    #################################################################
    #################################################################

    # build laplacian
    w = build_w(x, var)
    l_unn = build_laplacian(w, 'unn')
    l_norm = build_laplacian(w, laplacian_normalization)

    y_unn = spectral_clustering(l_unn, chosen_eig_indices)
    y_norm = spectral_clustering(l_norm, chosen_eig_indices)

    plot_clustering_result(x, y, l_unn, y_unn, y_norm, 1)


def parameter_sensitivity(laplacian_normalization='unn'):
    # parameter_sensitivity
    #       a skeleton function to test spectral clustering
    #       sensitivity to parameter choice

    # the number of samples to generate
    num_samples = 500

    #################################################################
    # choose the experiment parameter                               #
    #################################################################

    moon_var = 0.02  # exponential_euclidean's sigma^2

    # either 'unn'normalized, 'sym'metric normalization or 'rw' random-walk normalization
    # laplacian_normalization = 'unn'
    chosen_eig_indices = [1, 2]  # indices of the ordered eigenvalues to pick

    # the number of neighbours for the graph or the epsilon threshold
    parameter_candidate = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    parameter_performance = []
    #################################################################
    #################################################################

    for k in parameter_candidate:
        #        graph_param.graph_thresh = parameter_candidate(i);
        [x, y] = two_moons(num_samples, 1, moon_var)

        # automatically infer number of labels from samples
        num_classes = len(np.unique(y))

        w, similarity = build_similarity_graph(x, k=k)
        laplacian = build_laplacian(w, laplacian_normalization)

        y_rec = spectral_clustering(laplacian, chosen_eig_indices, num_classes)

        parameter_performance += [skm.adjusted_rand_score(y, y_rec)]

    plt.figure()
    plt.plot(parameter_candidate, parameter_performance)
    plt.title('parameter sensitivity')
    plt.show()


#########
# Tests #
#########

# two_blobs_clustering(True, 0.5)
# find_the_bend()
# find_the_bend(0.2)
# two_moons_clustering()
# point_and_circle_clustering('rw')
# point_and_circle_clustering('sym')
# parameter_sensitivity('sym')
# two_blobs_clustering(4)
